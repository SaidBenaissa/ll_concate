//
//  main.cpp
//  ll_concate
//
//  Created by SAID BENAISSA on 16/01/2021.
//

#include <iostream>
using namespace std;



struct Node {
    int data;
    struct Node * next;
}*first = NULL, *second = NULL, *third =NULL;

void create(int A[], int n){
    
    struct Node *t, *last;
    first = (struct Node *) malloc(sizeof(struct Node));
    first -> data = A[0];
    first -> next = NULL;
    last = first;

    for (int i =1; i<n; i++) {
        t = (struct Node *) malloc(sizeof(struct Node));
        t -> data = A[i];
        t -> next = NULL;
        last -> next = t;
        last = t;
        
    }
    
}

void create2(int A[], int n){
    
    struct Node *t, *last;
    second = (struct Node *) malloc(sizeof(struct Node));
    second -> data = A[0];
    second -> next = NULL;
    last = second;
    
    for (int i =1; i<n; i++) {
        t = (struct Node *) malloc(sizeof(struct Node));
        t -> data = A[i];
        t -> next = NULL;
        last -> next = t;
        last = t;
        
    }
    
}

void display(struct Node* p){
    while (p) {
        cout<<p->data<<" ";
        p = p -> next;
    }
    cout<<endl;
}

void concate(struct Node *p, struct Node *q) {
    third = p;
    
    while (p->next) {
        p = p->next;
    }
    p->next = q;
    
}

int main(int argc, const char * argv[]) {
    // insert code here...
    int A[] = {50, 40, 10, 30, 20};
    int B[] = {1, 2, 3, 4, 5};
    
    create(A, 5);
    display(first);
    
    create2(B, 5);
    display(second);
    
    concate(second , first);
    display(third);
    
    return 0;
}
